<?php

namespace Drupal\od_themed_routing\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\od_themed_routing\ThemeNegotiator;

/**
 * Controller routines for themed routing.
 *
 * @author h2b
 */
class ThemeController extends ControllerBase {

  /**
   * Adds a theme option and redirects to the original path..
   * @return string[]|\Drupal\Core\StringTranslation\TranslatableMarkup[]
   */
  public function control (string $theme, string $path1, string $path2, string $path3) {
    $path = '/' . implode('/', [$path1, $path2, $path3]);
    $logger = \Drupal::logger('routing');
    $logger->debug($this->t('Routing by @theme to: @path',
        [ '@theme' => $theme, '@path' => $path ]));
    $url = \Drupal::service('path.validator')->getUrlIfValid($path);
    if (! $url) throw new NotFoundHttpException(
        $this->t('Invalid path: @path', ['@path' => $path]));
    $name = $url->getRouteName();
    $params = $url->getRouteParameters();
    $options = $url->
        setOption('query', [ ThemeNegotiator::OPTION_THEMED_BY => $theme ])->
        getOptions();
    return $this->redirect($name, $params, $options);
  }

}
