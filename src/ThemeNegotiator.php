<?php

namespace Drupal\od_themed_routing;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Exception;

/**
 * Determines the active theme for a given route.
 *
 * @see https://www.webomelette.com/choose-your-theme-dynamically-drupal-8-theme-negotiation
 *
 */
class ThemeNegotiator implements ThemeNegotiatorInterface {

  /*
    Implementation note: Unfortunately, route options set by the controller do
    not survive until ihe route gets to this negotiator (at least, if it is a
    node route), so we cannot use them like the article cited above suggests. We
    use query parameters instead even though we need the request object for this.
  */

  const OPTION_THEMED_BY = 'theme';

  /**
   * {@inheritdoc}
   */
  public function applies (RouteMatchInterface $route_match) {
    try {
      $request = \Drupal::request();
      $query = $request->query;
      return $query->has($this::OPTION_THEMED_BY);
    } catch (Exception $e) {
      return false;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme (RouteMatchInterface $route_match) {
    try {
      $request = \Drupal::request();
      $query = $request->query;
      $theme = $query->get($this::OPTION_THEMED_BY, null);
      return $theme;
    } catch (Exception $e) {
      $logger = \Drupal::logger('routing');
      $logger->warning('Cannot determine active theme.');
      return null; //use default negotiator
    }
  }

}