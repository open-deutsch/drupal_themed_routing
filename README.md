# Open Deutsch Themed Routing

Provides different themes for each route.

Either use a path prefix

    /themed_by/THEME
    
or a query string

    ?theme=THEME
    
where `THEME` denotes the machine name of a theme (which must be installed and enabled, but of course need not be the default theme).

In the first form, the path that follows currently is restricted to a maximum of 3 slugs (the portion between two slashes). The second form has no such restriction.

## Examples

    /themed_by/redux/content/ueber-unser-portal

    /content/ueber-unser-portal?theme=redux

## Licence

Open Deutsch Themed Routing

Copyright 2019 Hans-Hermann Bode

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2,
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
